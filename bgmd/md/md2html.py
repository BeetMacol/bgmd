import regex as re
from .flavor import MdFlavor, HtmlTagStyle, MD_FLAVOR_NONE

BLOCK_REGEX = re.compile(r'^!([a-z0-9]+) ?{((?:.*\n)*?)(?:[^\\]?|\\\\)}')
LINK_REGEX = re.compile(r'(?:^|(?<=[^\\]|\\\\))\[(.*?)]\((.*?)\)')

class Md2HtmlConverter:
	def __init__(self, initial_flavor: MdFlavor = MD_FLAVOR_NONE) -> None:
		self.pos: int = 0
		self.md: str = ''
		self.html: str = ''

		self.flavor = initial_flavor
		self.custom_tags: dict[str, HtmlTagStyle] = {}

		self._allow_break = False
		self._empty_count = 0
		self._started_pg = False
		self._list_depth = 0

	def reset_parser(self) -> None :
		self._allow_break = True

	def put_md(self, md: str) -> object:
		self.pos = 0
		self.md = md
		self.reset_parser()
		return self
  
	def pull_html(self) -> str:
		html = self.html
		self.html = ''
		return html

	def _tag_open_start(self, name: str, **attribs) -> None:
		tag_style = HtmlTagStyle(name, [], {key: val for key, val in attribs.items()})
		if name in self.custom_tags:
			custom_tag = self.custom_tags[name]
			tag_style.tag = custom_tag.tag
			tag_style.attributes.update(custom_tag.attributes)
			if custom_tag.classes:
				tag_style.attributes['class'] = ' '.join(custom_tag.classes)
		self.html += '<' + name
		for key, val in tag_style.attributes.items():
			if key == 'class_':
				key = 'class'
			self.html += f' {key}="{val}"'
	def tag_open(self, name: str, **attribs) -> None:
		self._tag_open_start(name, **attribs)
		self.html += '>'
	def tag_close(self, name: str):
		self.html += '</' + name + '>'
	def tag_empty(self, name: str, **attribs) -> None:
		self._tag_open_start(name, **attribs)
		self.html += ' />'

	def done(self) -> bool:
		return self.pos >= len(self.md)
	# checks for data blocks; returns name and content
	def check(self) -> tuple[str, str] | None:
		match = re.match(BLOCK_REGEX, self.md[self.pos:])
		if match is None:
			return None
		self.pos += match.end()
		if self.md[self.pos] == '\n':
			self.pos += 1
		return (match.group(1), match.group(2))
	def line(self) -> object:
		md = self.md[self.pos:self.md.find('\n', self.pos)]
		size = len(md)
		if size == 0:
			self._empty_count += 1
			self.pos += 1
			return
		indentation = 0
		while md.startswith('  ' * (indentation + 1)):
			indentation += 1
		md = md.lstrip()
		# lists
		lists_unord: list[str] = []
		if self.flavor.lists_unord:
			lists_unord = self.flavor.lists_unord
		is_list_elem = False
		for marker in lists_unord:
			if md.startswith(marker + ' '):
				is_list_elem = True
				break
		if is_list_elem and indentation <= self._list_depth:
			if indentation == self._list_depth:
				self.tag_open('ul')
				self._list_depth += 1
			else:
				while indentation + 1 < self._list_depth:
					self.tag_close('ul')
					self._list_depth -= 1
			self.tag_open('li')
			is_list_elem = True
			md = md[2:]
		else:
			is_list_elem = False
			while self._list_depth > 0:
				self.tag_close('ul')
				self._list_depth -= 1
		# breaks and paragraphs
		if not is_list_elem:
			if self.flavor.paragraphs and self.flavor.paragraphs.pg_double_endl and self._empty_count == 1:
				if self._started_pg:
					self.tag_close('p')
				self.tag_open('p')
				self._started_pg = True
				self._allow_break = False
			elif self.flavor.paragraphs and self.flavor.paragraphs.pg_multiple_endl and self._empty_count > 1:
				if self._started_pg:
					self.tag_close('p')
				self.tag_open('p')
				self._started_pg = True
				self._allow_break = False
			elif self.flavor.paragraphs and self.flavor.paragraphs.br_double_space_start == True and self._allow_break and indentation >= 1:
				self.tag_empty('br')
				md = md[2:]
				self._allow_break = True
			else:
				self._allow_break = True
		self._empty_count = 0
		# headers
		header_size: int = 0
		while len(md) > header_size and md[header_size] == '#':
			header_size += 1
		if header_size and not is_list_elem and len(md) > header_size and md[header_size] == ' ':
			if self._started_pg:
				self.tag_close('p')
			md = md[header_size + 1:]
			self.tag_open(f'h{header_size}')
		elif header_size:
			header_size = 0
		# links
		if self.flavor.links:
			md = re.subf(LINK_REGEX, '<a href="{2}">{1}</a>', md) # TODO use custom format
		# normal content
		self.html += md
		# headers
		if header_size:
			self.tag_close(f'h{header_size}')
			self._empty_count = 1
		# lists
		if is_list_elem:
			self.tag_close('li')
		self.html += '\n'
		self.pos += size + 1
		return self
