from dataclasses import dataclass, field as dataclass_field

@dataclass
class HtmlTagStyle:
	tag: str = 'span'
	classes: list[str] = dataclass_field(default_factory=list)
	attributes: dict[str, str] = dataclass_field(default_factory=dict)
	content: str | None = None

@dataclass
class MdParagraphConfig:
	br_endl: bool				# emit <br> on endline (after non-empty line)
	br_double_space_end: bool	# end lines with double space for <br>
	br_double_space_start: bool	# start lines with double space for <br>
	br_double_endl: bool		# emit <br> on double endline
	pg_double_endl: bool		# start new paragraph on double endline
	pg_multiple_endl: bool		# start new paragraph triple+ endline 

@dataclass
class MdFlavor:
	paragraphs: MdParagraphConfig | None = None # TODO
	headers: bool = False
	header_links: HtmlTagStyle | None = None # TODO
	quotes: bool = False # TODO
	lists_unord: list[str] | None = None # TODO
	lists_ord: bool | list[str] = False # TODO
	code_blocks: bool = False # TODO
	span_styles: dict[str, HtmlTagStyle | str] = dataclass_field(default_factory=dict) # TODO
	links: bool = False # TODO
	auto_links: bool = False # TODO
	# TODO header ids ("#abc" after header)
	# TODO auto-escape < and & (always in code blocks and spans)

MD_FLAVOR_NONE = MdFlavor()

MD_FLAVOR_STANDARD = MdFlavor(
	paragraphs=MdParagraphConfig(False, True, False, False, True, True),
	headers=True,
	header_links=None,
	quotes=True,
	lists_unord=['*', '+', '-'],
	lists_ord=True,
	code_blocks=True,
	span_styles={
		'*': 'em',
		'**': 'strong',
		'_': 'em',
		'__': 'strong',
		'`': 'code',
		'```': 'code',
	},
	links=True,
	auto_links=True,
)

MD_FLAVOR_BEET = MdFlavor(
	paragraphs=MdParagraphConfig(False, False, True, True, True, True),
	headers=True,
	header_links=HtmlTagStyle('a', ['header-link']),
	quotes=True,
	lists_unord=['-'],
	lists_ord=['.'],
	code_blocks=True,
	span_styles={
		'*': 'em',
		'**': 'strong',
		'_': 'small',
		'__': 'u',
		'~': 's',
		'~~': 'del',
		'`': 'code',
		'^': 'sup',
		'^^': 'sub',
		'//': 'i',
		'\'\'': 'cite',
	},
	links=True,
	auto_links=True,
)
