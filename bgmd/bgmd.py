from pathlib import Path
from typing import Callable
import os
import shutil

class Bgmd:
	def __init__(self, output_path: Path | str, template_path: Path | str, input_path: Path | str, conver_callback: Callable[[str], str]):
		self.output_path = Path(output_path)
		template_file = open(template_path, 'r')
		self.template_str = template_file.read()
		template_file.close()
		self.input_path = Path(input_path)
		self.convert_callback = conver_callback
		if not self.output_path.exists():
			os.mkdir(self.output_path)

	def run(self):
		if (self.input_path / 'index.html').exists():
			shutil.copyfile(self.input_path / 'index.html', self.output_path / 'index.html')
		self.process_dir(Path(''))  

	def process_dir(self, dir: Path):
		real_dir = self.input_path / dir
		real_out_dir = self.output_path / dir
		if not real_out_dir.exists():
			os.mkdir(real_out_dir)
		if real_dir == self.output_path:
			return
		files = os.listdir(real_dir)
		for file in files:
			if os.path.isfile(real_dir / file):
				fpath = Path(file)
				if fpath.suffix == '.md':
					self.process_file(real_dir / fpath, real_out_dir / f'{fpath.stem}.html', (dir / f'{fpath.stem}.html').as_posix())
			else:
				self.process_dir(Path(dir / file))
		
	def process_file(self, input_path: Path, output_path: Path, web_path: str):
		print(f'Processing: {input_path}')
		in_text = open(input_path, 'r').read()
		in_text = in_text.replace('.md)', '.html)')
		process_text = self.convert_callback(in_text)
		html_text = self.template_str
		html_text = html_text.replace('[BGMD_CONTENT]', process_text)
		html_text = html_text.replace('[BGMD_WEB_PATH]', web_path)

		highest_web_dir_slash = web_path.find('/')
		html_text = html_text.replace('[BGMD_WEB_PATH_NO_ROOT]', web_path[(highest_web_dir_slash + 1):])
    
		open(output_path, 'w').write(html_text)
