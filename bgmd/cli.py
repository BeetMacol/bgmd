from argparse import ArgumentParser
from pathlib import Path
import bgmd
from .md.md2html import Md2HtmlConverter
from .md.flavor import MD_FLAVOR_STANDARD
from .md.flavor import MD_FLAVOR_BEET

def parser() -> ArgumentParser:
    parser = ArgumentParser(
        description='Gliko Markdown Deployer'
    )
    parser.add_argument('-t', '--template', type=lambda s: Path(s), default='template.html',
                        help='path to html template')
    parser.add_argument('-o', '--output', type=lambda s: Path(s), default='out',
                        help='path to output folder')
    parser.add_argument('-i', '--input', type=lambda s: Path(s), default='',
                        help='path to input folder')
    parser.add_argument('-f', '--flavor', type=lambda s: Path(s), default='standard',
                        help='flavor of markdown: standard, beet')
    
    return parser
    
def standard_convert_callback(input_str) -> str:
    conv = Md2HtmlConverter(MD_FLAVOR_STANDARD).put_md(input_str)
    while not conv.done(): conv.line()
    return conv.pull_html()

def beet_convert_callback(input_str) -> str:
    conv = Md2HtmlConverter(MD_FLAVOR_BEET).put_md(input_str)
    while not conv.done(): conv.line()
    return conv.pull_html()

def main() -> None:
    args = parser().parse_args()
    if args.flavor == "standard":
        gmd_ctx = bgmd.Bgmd(args.output, args.template, args.input, standard_convert_callback)
    else:
        gmd_ctx = bgmd.Bgmd(args.output, args.template, args.input, beet_convert_callback)
    
    gmd_ctx.run()
    print('Successfull deployment')

main()
