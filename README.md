# GBMD (~~Gliko~~ Beet Markdown Deployer)

This project was created as a fork of GMD, but quickly evolved into a flexible converter with an embedded Markdown parser. The new features allow embeding blocks of anything (text data of any kind) into the Markdown files, which can be used e.g. to produce complex or repeatable HTML code. The emphasis styling is also freely customizable.

Do not use the parser for user-generated content without any sanitization.

Btw, _~~**G**liko~~ **B**eet **M**arkdown **D**eployer_ is also totally known as _**G**ood**b**ye **M**arkdown **D**ownsides_, _**G**ood **B**ehaved **M**ark**d**own_ and _**G**BMD the **B**rave **M**arkdown Re**d**esign_.

## Markdown features

Most of the Markdown flavor is customizable, but not everything is possible. 

Fully breaking original conventions, ideas or even philosophy of Markdown is welcome, my flavor does that too. I, for example, do not agree that hard-wrapping is important enough to force everyone to put two spaces at the end of a line to make an endline work. Moreover, I consider that practise uglier than non-wrapped text. Therefore, I prefer flavors treating line breaks as line breaks. After some considerations, however, I decided for my flavor to be a sort of compromise – it uses the ugly double space, but makes it less ugly (for my eyes at least) by putting it at the beginning of the next line.

| Standard | Feature | Support | Customizability | Notes |
|:-:|:-:|:-:|:-:|:-:|
| Original | Flexible data blocks | — | None in syntax, otherwise infinite | Handled by your code; use for anything |
| Official | Paragraphs | Full | Most prefrences covered | This includes the above discussion |
| Official | Headers | Full | None (on/off) | Six HTML header sizes |
| Original | Header IDs | — | None (on/off) | Set header ID by putting `#the-id` at its end |
| Original | Header Links | — | Any HTML tag and attributes | An option to put links next to every header for easy URL-grab |
| Official | Blockquotes | Planned | — | |
| Official | Unordered Lists | Planned | List marker selection | |
| Official | Ordered Lists | Planned | Can use one-character markers instead of numbers | For now, can't enable both modes at the same time |
| Official | Code Blocks | Planned | None (on/off) | These are the TAB code blocks, not `inline` ones |
| Official | Horizontal Rules | No | — | Just use `<hr />`... |
| Official | Emphasis/Code Spans | Full | Any HTML tag and attributes, any characters | These are very customizable |
| Official | Links | Partial | — | For now, reference links are unsupporeted |
| Official | Automatic Links | Planned | — | The simple url and e-mail addresses encslosed in `<>` |
| Official | Images | Planned | — | |
| Official | Auto-escape | Planned | — | Auto-escaping `<` and `&` |
| Common | Tables | Planned | — | Many variants exist |
| Common | Notes | Planned | — | May be added with reference links |
